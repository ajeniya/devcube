import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './ui/home/home.component';
import { SigninComponent } from './ui/signin/signin.component';
import { SignupComponent } from './ui/signup/signup.component';
import { ProfileComponent } from './ui/profile/profile.component';
import { LogoutComponent } from './ui/logout/logout.component';
import { ActivateEmailComponent } from './ui/activate-email/activate-email.component';
import { DocumentComponent } from './document/document.component';


import { AuthGuard } from './services/auth-guard.service';
import { LogoutGuardService } from './services/logout-guard.service';
import { PageNotFoundComponent } from './ui/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'document', component: DocumentComponent },
  {
    path: 'chat',
    loadChildren: '../app/chat/chat.module#ChatModule'
  },
  {
    path: 'article',
    loadChildren: '../app/article/article.module#ArticleModule'
  },
  {
    path: 'question',
    loadChildren: '../app/question/question.module#QuestionModule'
  },
  { path: 'activate-email', component: ActivateEmailComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
