import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { QuillModule } from 'ngx-quill';
import { TagInputModule } from 'ngx-chips';
import { HighlightJsModule } from 'ngx-highlight-js';
import { ImageCropperModule } from 'ngx-image-cropper';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { AvatarModule } from 'ngx-avatar';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

const config: SocketIoConfig = { url: 'http://localhost:3000', options: {} };

import { NbThemeModule } from '@nebular/theme';
import { NbSidebarModule, NbButtonModule, NbInputModule,
NbCardModule, NbActionsModule, NbAlertModule,
NbMenuModule, NbContextMenuModule, NbSelectModule,
NbLayoutModule, NbSpinnerModule, NbUserModule,
NbDialogModule, NbCheckboxModule,
NbSidebarService } from '@nebular/theme';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';

import { AppErrorHander } from './common/app-error-handler';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './ui/header/header.component';
import { FooterComponent } from './ui/footer/footer.component';
import { HomeComponent } from './ui/home/home.component';
import { PageNotFoundComponent } from './ui/page-not-found/page-not-found.component';
import { SigninComponent } from './ui/signin/signin.component';
import { SignupComponent } from './ui/signup/signup.component';
import { AuthGuard } from './services/auth-guard.service';
import { LogoutGuardService } from './services/logout-guard.service';

import { ProfileComponent } from './ui/profile/profile.component';
import { ActivateEmailComponent } from './ui/activate-email/activate-email.component';
import { LogoutComponent } from './ui/logout/logout.component';
import { AuthInterceptor } from './services/auth.interceptors';
import { ChatComponent } from './ui/chat/chat.component';
import { DocumentListComponent } from './document-list/document-list.component';
import { DocumentComponent } from './document/document.component';

import { ChatModule } from '../app/chat/chat.module';
import { NgChatModule } from 'ng-chat';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    PageNotFoundComponent,
    SigninComponent,
    SignupComponent,
    ProfileComponent,
    ActivateEmailComponent,
    LogoutComponent,
    ChatComponent,
    DocumentListComponent,
    DocumentComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NbLayoutModule,
    NbCheckboxModule,
    NbSidebarModule,
    NbActionsModule,
    NbAlertModule,
    NbSelectModule,
    NbCardModule,
    NbUserModule,
    NbDialogModule.forRoot(),
    NbButtonModule,
    NbSpinnerModule,
    NbInputModule,
    NbContextMenuModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NbMenuModule.forRoot(),
    NbThemeModule.forRoot({ name: 'default' }),
    SocketIoModule.forRoot(config),
    SnotifyModule,
    FormsModule,
    QuillModule,
    TagInputModule,
    HighlightJsModule,
    ImageCropperModule,
    Ng2ImgMaxModule,
    AvatarModule,
    SnotifyModule,
    NgChatModule
    ],
  providers: [NbSidebarService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService,
    AuthGuard,
    LogoutGuardService,
    {provide: String, useValue: "dummy"},
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
