import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor( private dataServices: DataService, private router: Router ) {

  }

  ngOnInit() {
    this.dataServices.updateLoggedOut();

    this.dataServices.getLoggedInStatus.emit(false);

    this.dataServices.logout();
    location.reload(true);
    this.router.navigate(['/home']);
  }

}
