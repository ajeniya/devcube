import { Component, Inject, ChangeDetectionStrategy, ChangeDetectorRef, OnInit } from '@angular/core';
import { NB_WINDOW, NbMenuService } from '@nebular/theme';
import { filter, map } from 'rxjs/operators';

import { Subscription } from 'rxjs';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: any;
  checkLoggedin: any;
  message: any;
  subscription: any;
  isUserLoggedIn: boolean;
  loggedIn: any;
  token: string;

  user: string;

  items = [
    { title: 'Profile', link: ['/profile'] },
    { title: 'Logout', link: ['/logout'] },
  ];

  constructor(private dataServices: DataService, private cdr: ChangeDetectorRef,
    private nbMenuService: NbMenuService, @Inject(NB_WINDOW) private window ) {
      this.dataServices.getLoggedInStatus.subscribe(data => this.loggedStatus(data));
  }

  private loggedStatus(data) {
    console.log(data);
    this.loggedIn = data;
}

  ngOnInit() {

    // this.dataServices.cast.subscribe(user => this.user = user);

    this.token = this.dataServices.getAuthToken;
      if ( this.token ) {
        console.log( this.token );
        return this.loggedIn = true;
      }

    this.nbMenuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'my-context-menu'),
        map(({ item: { title } }) => title),
      )
      .subscribe(title => this.window.alert(`${title} was clicked!`));
  }

}
