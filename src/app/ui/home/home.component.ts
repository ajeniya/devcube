import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isLoggedIn;

  constructor( private service: DataService ) {
    //this.isLoggedIn = service.isLoggedIn();
  }

  ngOnInit() {
  }

}
