import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { trigger, style, animate, transition } from '@angular/animations';
import {Router} from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { HomeComponent } from '../home/home.component';
import { RoutingStateService } from '../../services/routing-state.service';

import { NotFoundError } from '../../common/not-found-error';
import { BadInput } from '../../common/bad-input';
import { AppError } from '../../common/app-error';
import { throwError } from 'rxjs';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(500)
      ]),

      transition('* => void', [
        animate(500, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class SigninComponent implements OnInit {
  users: any[];
  signinForm: FormGroup;
  loading = false;
  connectionError = false;
  appError: string;
  emailOrPasswordNotFound: boolean;
  returnData: string;
  inValidPasswordError: boolean;
  previousRoute;
  loggedInToken: any;
  user: string;
  editUser: string;

  constructor(private fb: FormBuilder, private dataServices: DataService,
    private routingState: RoutingStateService,
    private service: AuthService, private router: Router) {
    this.signinForm = this.signinFormGroup();
   }


   signinFormGroup() {
    return new FormGroup({
      email: new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])),
      password: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {

    this.dataServices.cast.subscribe(user => this.user = user);

    if (this.dataServices.isLoggedIn()) {
      // this.previousRoute = this.routingState.getPreviousUrl();
      // return true;

      this.router.navigate(['/home']);
    }
  }

  editTheUser() {
    this.dataServices.editUser(this.editUser);
  }

  onClose() {
    this.connectionError = false;
  }

  onSubmitSignin() {
    this.loading = true;
    console.log(this.signinForm.value);
    this.service.login(this.signinForm.value).subscribe(
      returnData => {
          this.loggedInToken = returnData;
        // show notification for account onsuccessfull signup
 
        this.loading = false;
        this.signinForm.reset();

        this.dataServices.getLoggedInStatus.emit(true);

        // close notification panel
        this.connectionError = false;

        // store return data into localstorage
        const storeTokenInLocalStorage = localStorage.setItem('x-auth-token', this.loggedInToken.token);
        this.dataServices.updateLoggedIn();
        this.router.navigate(['/home']);

      },
      (error: AppError) => {
        this.loading = false;
        if (error instanceof NotFoundError) {

          this.connectionError = true;
          this.appError = error.originalError.error;

        } else {

          if ( error.originalError) {
            console.log(error)

            if ( error.originalError.error.text ) {
              this.appError = error.originalError.error.text;
              this.connectionError = true;
              return true;
            }

            this.appError = error.originalError.error;
            this.connectionError = true;
             return true;
          }
          this.appError = error.originalError.error.text;
          this.connectionError = true;
          throwError(error);
        }
      });
  }

  get email() {
    return this.signinForm.get('email');
  }

  get password() {
    return this.signinForm.get('password');
  }

}
