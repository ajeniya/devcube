import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { trigger, style, animate, transition } from '@angular/animations';
import {Router} from '@angular/router';

import { UserService } from '../../services/user.service';
import { DataService } from '../../services/data.service';
import { NotFoundError } from '../../common/not-found-error';
import { BadInput } from '../../common/bad-input';
import { AppError } from '../../common/app-error';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(500)
      ]),

      transition('* => void', [
        animate(500, style({ opacity: 0 }))
      ])
    ])
  ]
})

export class SignupComponent implements OnInit {
  users: any[];
  userForm: FormGroup;
  loading = false;
  connectionError = false;
  appError: string;
  emailExistViewError = false;
  emailExistError: string;
  onSuccessfulSignup = false;

  constructor(private fb: FormBuilder, private service: UserService,
    private dataServices: DataService,  private router: Router) {
    this.userForm = this.createFormGroup();
  }

  createFormGroup() {
    return new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])),
      password: new FormControl('', Validators.required)
    });
  }

  onClose() {
    this.connectionError = false;
    this.emailExistViewError = false;
    this.onSuccessfulSignup = false;
  }

  onSubmitSignUp() {
    this.loading = true;

    this.service.create(this.userForm.value).subscribe(
      returnData => {
        // show notification for account onsuccessfull signup
        this.onSuccessfulSignup = true;

        this.loading = false;
        this.userForm.reset();

        // close notification panel
        this.emailExistViewError = false;
        this.connectionError = false;

        // store return data into localstorage
        localStorage.setItem('x-auth-token', returnData[0] );

      },
      (error: AppError) => {
        this.loading = false;
        if (error instanceof NotFoundError) {
          this.emailExistError = error.originalError.error;
          this.emailExistViewError = true;

        } else {
          this.appError = error.originalError.error;
          this.connectionError = true;
          throwError(error);
        }
      });
  }

  get name() {
    return this.userForm.get('name');
  }

  get email() {
    return this.userForm.get('email');
  }

  get password() {
    return this.userForm.get('password');
  }

  ngOnInit() {
    if (this.dataServices.isLoggedIn()) {

      this.router.navigate(['/home']);
    }
  }

  createUser(input: HTMLInputElement) {
    const user = { title: input.value };
    input.value = '';

    this.service.create(user)
      .subscribe(
        newuser => {
           alert(newuser);
          },
          (error: AppError) => {
            if (error instanceof BadInput) {
            } else {
              throw error;
            }
          });
  }

  updateUser(user) {
    this.service.update(user)
      .subscribe(
        updateduser => {
          console.log(updateduser);
        });
  }


  deleteUser(user) {
    this.service.delete(user.id)
      .subscribe(
        () => {
          const index = this.users.indexOf(user);
          this.users.splice(index, 1);
        },
        (error: AppError) => {
          if (error instanceof NotFoundError) {
            alert('This user has already been deleted.');
          } else {
            throw error;
          }
        });
  }

}
