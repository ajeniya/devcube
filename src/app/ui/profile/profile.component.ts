import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import {SnotifyService} from 'ng-snotify';

import 'rxjs/add/operator/filter';
import { NotFoundError } from '../../common/not-found-error';
import { BadInput } from '../../common/bad-input';
import { AppError } from '../../common/app-error';
import { throwError } from 'rxjs';
import { UserService } from '../../services/user.service';
import { CountryService } from '../../services/country.service';
import { StateService } from '../../services/state.service';
import { CityService } from '../../services/city.service';
import { DataService } from '../../services/data.service';
import { AvaterService } from '../../services/avater.service';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { DomSanitizer } from '@angular/platform-browser';

import {Router} from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileUpdateForm: FormGroup;
  emailUpdateForm: FormGroup;
  messageUpdateForm: FormGroup;
  deleteProfileForm: FormGroup;
  viewMode = '';
  loading = false;
  activated: boolean;
  items: any;
  showCropSection: boolean;
  uploadedImage: File;
  imagePreview: any;
  uploadSpinner: boolean;
  uploadPayLoad: any;
  imageAfterUpload: any;
  allCountry: any;
  disableStateForm = false;
  selectedCountryState: any;
  selectedStateCity: any;
  checkSetting = true;
  emailPrivacySetting;
  userObject: any;
  tagInput: any;
  developer_approved: boolean;
  article_approved: boolean;
  comment_approved: boolean;
  selectedCountry: string;
  selectedState: string;
  selectedCity: string;
  selectedWorkout = 'Testing frm';
  editLocation: boolean = false;

  email: any;
  email_comment_article: any;
  email_comment_question: any;



  constructor(private route: ActivatedRoute, private router: Router, 
    private fb: FormBuilder,
    private ng2ImgMax: Ng2ImgMaxService, public sanitizer: DomSanitizer,
    private stateServices: StateService, private cityServices: CityService,
    private cdr: ChangeDetectorRef, private dataServices: DataService,
    private snotifyService: SnotifyService,
    private avaterServices: AvaterService, public countryServices: CountryService,
    private service: UserService, private dialogService: NbDialogService) {
    }

     ngOnInit() {
      this.profileUpdateForm = this.profileUpdateFormGroup();
      this.emailUpdateForm = this.emailUpdateFormGroup();
      this.messageUpdateForm = this.messageUpdateFormGroup();
      this.deleteProfileForm = this.deleteProfileFormGroup();
      
      this.cdr.detectChanges();
      this.viewMode = 'profile';

      this.dataServices.userRecord()
        .subscribe((userData) => {
          this.userObject = userData;
          console.log(this.userObject);
          const tags = this.userObject.profile.technology_tag;
          this.tagInput = JSON.parse(tags);

          this.email = (this.userObject.email_approved == 1 ) ? true : false;
          this.email_comment_article = (this.userObject.email_comment_article == 1 ) ? true : false;
          this.email_comment_question = (this.userObject.email_comment_question == 1 ) ? true : false;

          this.developer_approved = (this.userObject.developer_approved == 1 ) ? true : false;
          this.article_approved = (this.userObject.article_approved == 1 ) ? true : false;
          this.comment_approved = (this.userObject.comment_approved == 1 ) ? true : false;
        });



       this.countryServices.getAll()
       .subscribe((response) => {
         this.allCountry = response;
       });

      this.profileUpdateForm.get('country').valueChanges.subscribe(value => {
          this.selectedCountry = value.name;
          this.stateServices.getState(value.id)
          .subscribe((response) => {
            this.selectedCountryState = response;
          });
       });
  
      this.profileUpdateForm.get('state').valueChanges.subscribe(value => {
        this.selectedState = value.name;
        this.cityServices.getCity(value.id)
        .subscribe((response) => {
          this.selectedStateCity = response;
        });
     });

    this.profileUpdateForm.get('city').valueChanges.subscribe(value => {
      this.selectedCity = value.name;
      this.cityServices.getCity(value.id)
      .subscribe((response) => {
        this.selectedStateCity = response;
      });
   });
    }

    onEdit() {
      this.editLocation = true;
    }


    profileUpdateFormGroup() {
      return new FormGroup({
        title: new FormControl('', Validators.required),
        country: new FormControl('', Validators.required),
        state: new FormControl('', Validators.required),
        city: new FormControl('', Validators.required),
        name: new FormControl('', Validators.required),
        technologies: new FormControl('', Validators.required),
        aboutMe: new FormControl('', Validators.required),
      });
    }
  
    emailUpdateFormGroup() {
      return new FormGroup({
        email: new FormControl('', Validators.compose([Validators.required])),
        email_comment_article: new FormControl('', Validators.compose([Validators.required])),
        email_comment_question: new FormControl('', Validators.compose([Validators.required])),
      });
    }
  
    messageUpdateFormGroup() {
      return new FormGroup({
        developer: new FormControl('', Validators.required),
        article: new FormControl('', Validators.required),
        comment: new FormControl('', Validators.required)
      });
    }
  
    deleteProfileFormGroup() {
      return new FormGroup({
        deleteProfile: new FormControl('', Validators.required)
      });
    }
  
    onEmailSettingProfile() {
      this.loading = true;
      this.service.emailPrivacy(this.emailUpdateForm.value)
      .subscribe((result) => {
        this.loading = false;
  
        this.snotifyService.success('Email setting updated', {
          timeout: 2000,
          showProgressBar: false,
          closeOnClick: false,
          pauseOnHover: true
        });
      });
    }
  
    onMessageSettingProfile() {
      this.loading = true;
      this.service.messagePrivacy(this.messageUpdateForm.value)
      .subscribe((result) => {
        this.loading = false;
  
        this.snotifyService.success('Messages setting updated', {
          timeout: 2000,
          showProgressBar: false,
          closeOnClick: false,
          pauseOnHover: true
        });
      });
    }
  
    onDeleteProfile() {
      this.snotifyService.confirm('Are you sure you want to delete your profile', 'Delete profile ', {
        timeout: 5000,
        showProgressBar: true,
        closeOnClick: false,
        pauseOnHover: true,
        buttons: [
          {text: 'Yes', action: () => this.deleteAccount(), bold: false},
          {text: 'No', action: () => console.log('Clicked: No')},
          {text: 'Close', action: (toast) => {console.log('Clicked: No'); this.snotifyService.remove(toast.id); }, bold: true},
        ]
      });
    }
  
    deleteAccount() {
      this.service.deleteAccount();
    }
  
  
    onImageChange(event) {
      const image = event.target.files[0];
      this.uploadSpinner = true;
  
      this.ng2ImgMax.resizeImage(image, 10000, 300).subscribe(
        result => {
          this.uploadedImage = new File([result], result.name);
          this.getImagePreview(this.uploadedImage);
  
          const id: any = 91;
  
          const uploadData = new FormData();
          uploadData.append('avater', image, image.name);
          uploadData.append('id', id );
  
          this.avaterServices.avater( uploadData ).subscribe((data) => {
  
            this.imageAfterUpload = data;
          });
  
        },
        error => {
          console.log('😢 Oh no!', error);
        }
      );
    }
  
    getImagePreview(file: File) {
      const reader: FileReader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.uploadSpinner = false;
        this.imagePreview = reader.result;
      };
    }
  
    onUpdateProfile() {
      this.loading = true;
      this.service.updateProfile(this.profileUpdateForm.value)
      .subscribe((result) => {
        this.loading = false;
        this.snotifyService.success('Profile updated', {
          timeout: 2000,
          showProgressBar: false,
          closeOnClick: false,
          pauseOnHover: true
        });
      });
    }
  
}
