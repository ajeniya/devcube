import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';

import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit/edit.component';
import { DetailComponent } from './detail/detail.component';
import { AddComponent } from './add/add.component';

import { LMarkdownEditorModule } from 'ngx-markdown-editor';


export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'edit', component: EditComponent },
  { path: 'detail/:id', component: DetailComponent, pathMatch: 'full' },
  { path: 'add', component: AddComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
    LMarkdownEditorModule
  ],
  exports: [RouterModule]
})
export class QuestionRoutingModule { }
