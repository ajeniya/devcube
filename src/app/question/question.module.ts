import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { QuestionRoutingModule } from './question-routing.module';
import { HomeComponent } from './home/home.component';
import { AddComponent } from './add/add.component';
import { DetailComponent } from './detail/detail.component';
import { EditComponent } from './edit/edit.component';
import { LMarkdownEditorModule } from 'ngx-markdown-editor';
import { NbThemeModule } from '@nebular/theme';
import { NbSidebarModule, NbButtonModule, NbInputModule,
NbCardModule, NbActionsModule, NbAlertModule,
NbMenuModule, NbContextMenuModule, NbSelectModule,
NbLayoutModule, NbSpinnerModule, NbUserModule,
NbDialogModule, NbCheckboxModule,
NbSidebarService } from '@nebular/theme';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { TagInputModule } from 'ngx-chips';
import { MarkdownModule, MarkedOptions } from 'ngx-markdown';
import { MomentModule } from 'angular2-moment';
import { QuillModule } from 'ngx-quill';


@NgModule({
  declarations: [HomeComponent, AddComponent, DetailComponent, EditComponent],
  imports: [
    CommonModule,
    QuestionRoutingModule,
    LMarkdownEditorModule,
    FormsModule,
    ReactiveFormsModule,
    NbSpinnerModule,
    NbButtonModule,
    NbLayoutModule,
    TagInputModule,
    MarkdownModule.forRoot(),
    MomentModule,
    QuillModule
  ]
})
export class QuestionModule { }
