import { Component, OnInit } from '@angular/core';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  allQuestion: any;

  constructor(public questionServices: QuestionService) { }

  ngOnInit() {
    this.questionServices.getAllQuestion()
    .subscribe((result) => {
      this.allQuestion = result;
      console.log(result);
    })
  }

}
