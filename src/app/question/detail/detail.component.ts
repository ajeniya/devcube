import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {SnotifyService} from 'ng-snotify';

import {Router} from '@angular/router';


import { QuestionService } from 'src/app/services/question.service';
import { AnswerService } from '../../services/answer.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {
  id: number;
  private sub: any;
  private question: any;
  private questionComments: any;
  private questionCommentSub: any;
  private singleQuestion: any;
  private addComment = false;
  private loading: any;
  private resultObject: any;
  private questionUpvote: any;
  private questionDownvote: any;
  private questionAnswer: any;


  public upvote = 0;
  public downvote = 0;

  constructor( private route: ActivatedRoute, public questionServices: QuestionService,
    public answerServices: AnswerService, 
    private router: Router, private snotifyService: SnotifyService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.id = +params['id']; // (+) converts string 'id' to a number

       this.singleQuestion = this.questionServices.getSingleQuestion(this.id)
        .subscribe((result) => {

        this.getSingleCmt();
        this.question = result;
        });
    });
    this.getQuestionAnswer();
    
  }

  


  getSingleCmt() {
    this.questionCommentSub = this.questionServices.getSingleQuestioncomment(this.id)
    .subscribe((questioncomment) => {
      this.questionComments = questioncomment;
    });
  }
  // goToProductDetails(id) {
  //   this.router.navigate(['/product-details', id]);
  // }

  getQuestionAnswer() {
    console.log(this.id);
    this.answerServices.getSingleanswer(this.id)
      .subscribe((data) => {
        this.questionAnswer = data;
        console.log(data);
      });
  }

  addAnswerComment() {
    this.addComment = true;
  }


  addQustionAnswer(i) {
    console.log(i);
    this.addComment = true;
  }

  onsubmitanswercomment(form: NgForm) {
    if (form.valid) {

      const postObject = {
        question_id : this.id,
        comment_question : form.value.comment
      }

      this.addComment = false;

      this.loading = true;
      this.questionServices.addSingleQuestioncomment(postObject)
      .subscribe((result) => {
        this.resultObject = result;
        this.loading = false;
        form.form.reset();
        this.getSingleCmt();
        this.snotifyService.success('Comment added', {
          timeout: 1000,
          showProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true
        });
      });
    }
  }


  onsubmitquestionanswercomment(form: NgForm) {
    if (form.valid) {

      const postObject = {
        question_id : this.id,
        comment_question : form.value.comment
      }

      console.log(postObject);

      // this.addComment = false;

      // this.loading = true;
      // this.questionServices.addSingleQuestioncomment(postObject)
      // .subscribe((result) => {
      //   this.resultObject = result;
      //   this.loading = false;
      //   form.form.reset();
      //   this.getSingleCmt();
      //   this.snotifyService.success('Comment added', {
      //     timeout: 1000,
      //     showProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true
      //   });
      // });
    }
  }

  onsubmitquestionanswer(form: NgForm) {
    if (form.valid) {

      const postObject = {
        question_id : this.id,
        answer : form.value.answer
      }

      console.log(postObject);

      this.loading = true;
      this.answerServices.addAnswer(postObject)
      .subscribe((result) => {
        this.resultObject = result;
        this.loading = false;
        form.form.reset();
        this.getSingleCmt();
        this.getQuestionAnswer();
        this.snotifyService.success('Answer added', {
          timeout: 1000,
          showProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true
        });
      });
    }
  }


  trackByFn(index, questionComment) {
    return questionComment.id;
  }

  trackByFnQuestionAnswer(index, questionComment) {
    return questionComment.id;
  }

  update(item, questionComment) {
    console.log(questionComment);
  }

  updateDownvote(item, questionComment) {
    item.downvote ++;
    console.log(item.id);
    const param = {'answerCommentId': item.id};
    this.questionServices.questionCommentDownvote(param)
      .subscribe((data) => {
         this.questionUpvote = data;
         this.snotifyService.success('Upvoted', {
          timeout: 1000,
          showProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true
        });
      });
  }

  updateUpvote(item, questionComment) {
    item.upvote ++;
    console.log(item.id);
    const param = {'answerCommentId': item.id};
    this.questionServices.questionCommentUpvote(param)
      .subscribe((data) => {
         this.questionUpvote = data;
         this.snotifyService.success('Downvoted', {
          timeout: 1000,
          showProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true
        });
      });
  }

  updateQuestionUpvote(value) {
    const param = {'id': value.id};
    this.questionServices.questionUpvote(param)
      .subscribe((data) => {
         this.questionUpvote = data;
         this.snotifyService.success('Upvoted', {
          timeout: 1000,
          showProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true
        });
      });
  }

  updateQuestionDownvote(value) {
    const param = {'id': value.id};
    this.questionServices.questionDownvote(param)
      .subscribe((data) => {
         this.questionUpvote = data;
         this.snotifyService.success('Downvoted', {
          timeout: 1000,
          showProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true
        });
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.singleQuestion.unsubscribe();
    this.questionCommentSub.unsubscribe();
  }



}
