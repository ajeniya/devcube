import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {SnotifyService} from 'ng-snotify';

import {Router} from '@angular/router';
import { QuestionService } from 'src/app/services/question.service';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  addQuestionForm: FormGroup;
  loading: boolean;
  allQuestion: any;

  constructor(private route: ActivatedRoute, private router: Router,
    private snotifyService: SnotifyService,
    public questionServices: QuestionService,
    private formBuilder: FormBuilder) {
    }


  ngOnInit() {
    this.addQuestionForm = this.formBuilder.group({
      titleBody: ['', Validators.required],
      question: ['', Validators.required],
      tags: ['', [Validators.required]],
    });

    this.questionServices.getAllQuestion()
    .subscribe((result) => {
      this.allQuestion = result;
      console.log(result);
    })
  }




  onAddQuestion() {
    this.loading = true;
    console.log(this.addQuestionForm.value);
    this.questionServices.addQuestion(this.addQuestionForm.value)
    .subscribe((result) => {
      this.loading = false;
      this.snotifyService.success('Profile updated', {
        timeout: 2000,
        showProgressBar: false,
        closeOnClick: false,
        pauseOnHover: true
      });
    });
  }

}
