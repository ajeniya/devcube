import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import {SnotifyService} from 'ng-snotify';

import { ArticleService } from '../../services/article.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-myarticle',
  templateUrl: './myarticle.component.html',
  styleUrls: ['./myarticle.component.scss']
})
export class MyarticleComponent implements OnInit {
  allArticles;
  
  constructor( private articleServices: ArticleService,
    private fb: FormBuilder,
    private snotifyService: SnotifyService,
    ) { 
    }


  ngOnInit() {
    this.articleServices.getAll()
    .subscribe((response) => {
      this.allArticles = response;
    });
  }

}
