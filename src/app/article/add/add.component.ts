import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { SnotifyService } from 'ng-snotify';
import UIkit from 'uikit';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, TableService } from '@syncfusion/ej2-angular-richtexteditor';

import { ArticleService } from '../../services/article.service';
import { RichTextEditor, Toolbar, Image,  Link, HtmlEditor, QuickToolbar, NodeSelection } from '@syncfusion/ej2-richtexteditor';
RichTextEditor.Inject(Toolbar, Image,  Link, HtmlEditor, QuickToolbar );

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})

export class AddComponent implements OnInit {
  

  private disableButton: boolean = false;
  addArticleForm: FormGroup;
  allArticles: any;
  loading = false;
  toolbarOptions;

    
 defaultRTE: RichTextEditor = new RichTextEditor({
      toolbarSettings: {
      items: ['Image']}
});


  constructor( private articleServices: ArticleService,
    private fb: FormBuilder, private _elementRef: ElementRef,
    private snotifyService: SnotifyService,
    ) {
        this.addArticleForm = this.addArticleFormGroup();
    }

  addArticleFormGroup() {
      return new FormGroup({
        title: new FormControl('', Validators.required),
        article: new FormControl('', Validators.required),
        techTag: new FormControl('', Validators.required)
      });
    }

  ngOnInit() {
    this.articleServices.getAll()
    .subscribe((response) => {
      this.allArticles = response;
    });

   
  }

  showAlert(): void {
    UIkit.modal.confirm('UIkit confirm!').then(function() {
        console.log('Confirmed.')
    }, function () {
        console.log('Rejected.')
    });
  }

  onAddArticle() {
    this.loading = true;
    this.disableButton = true;
    console.log(this.addArticleForm.value)
    this.articleServices.create(this.addArticleForm.value)
    .subscribe((result) => {
      this.loading = false;
      this.disableButton = false;
      this.snotifyService.success('Your article as been saved, Publish your article now', {
        timeout: 2000,
        showProgressBar: false,
        closeOnClick: false,
        pauseOnHover: true
      });
    });
  }

}
