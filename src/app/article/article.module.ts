import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { QuillModule } from 'ngx-quill';
import { TagInputModule } from 'ngx-chips';

import { ArticleRoutingModule } from './article-routing.module';
import { HomeComponent } from './home/home.component';
import { DetailComponent } from './detail/detail.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { MyarticleComponent } from './myarticle/myarticle.component';

import { LMarkdownEditorModule } from 'ngx-markdown-editor';
import { NgxEditorModule } from 'ngx-editor';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';


import { NbThemeModule } from '@nebular/theme';
import { NbSidebarModule, NbButtonModule, NbInputModule,
NbCardModule, NbActionsModule, NbAlertModule,
NbMenuModule, NbContextMenuModule, NbSelectModule,
NbLayoutModule, NbSpinnerModule, NbUserModule,
NbDialogModule, NbCheckboxModule,
NbSidebarService } from '@nebular/theme';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';


@NgModule({
  declarations: [HomeComponent, DetailComponent, AddComponent, EditComponent, MyarticleComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ArticleRoutingModule,
    QuillModule,
    TagInputModule,
    NbLayoutModule,
    NbCheckboxModule,
    NbSidebarModule,
    NbActionsModule,
    NbAlertModule,
    NbSelectModule,
    NbCardModule,
    NbUserModule,
    NbDialogModule.forRoot(),
    NbButtonModule,
    NbSpinnerModule,
    NbInputModule,
    NbContextMenuModule,
    LMarkdownEditorModule,
    NgxEditorModule,
    RichTextEditorAllModule
  ]
})
export class ArticleModule { }
