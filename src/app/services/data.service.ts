import { HttpClient , HttpEvent, HttpHeaders, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import { AppError } from '../common/app-error';
import { NotFoundError } from '../common/not-found-error';
import { BadInput } from '../common/bad-input';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  @Output() getLoggedInStatus: EventEmitter<any> = new EventEmitter();
  public endpoint = 'http://localhost:3000/api/';

  private isUserLoggedIn = new BehaviorSubject<boolean>(false);
  checkLoggedIn = this.isUserLoggedIn.asObservable();

  private user = new BehaviorSubject<string>('');
  cast = this.user.asObservable();

  getSignupToken = localStorage.getItem('x-signup-token');
  getAuthToken = localStorage.getItem('x-auth-token');


constructor(public url: string, public http: HttpClient) {
}

  editUser(newUser) {
    this.user.next(newUser);
  }

  updateLoggedIn() {
    this.isUserLoggedIn.next(true);
  }

  updateLoggedOut() {
    this.isUserLoggedIn.next(false);
  }


  isLoggedIn() {
    const helper = new JwtHelperService();
    const RawToken = this.getAuthToken;

    if (!RawToken) {
      return false;
      // After the user has logged in, emit the behavior subject changes.
    }

    const expirationDate = helper.getTokenExpirationDate(RawToken);
    const isExpired = helper.isTokenExpired(RawToken);
    const decodeToken = helper.decodeToken(RawToken);

    return true;
  }

  getCurrentUser() {
    const Authtoken = this.getAuthToken;
    if (!Authtoken) {
      return null;
    }

    const jwtHelper = new JwtHelperService();
    return jwtHelper.decodeToken(Authtoken);

  }

  logout() {
      localStorage.removeItem('x-signup-token');
      localStorage.removeItem('x-auth-token');
      localStorage.clear();
  }

  getAll() {
    return this.http.get(this.url)
      .map(response => response)
      .catch(this.handleError);
  }

  userRecord() {
    return this.http.get(`${this.endpoint}user/me`)
      .map(response => response)
      .catch(this.handleError);
  }


  create(resource) {
    return this.http.post(this.url, resource)
      .map(response => response)
      .catch(this.handleError);
  }


  activateEmail(resource) {
    console.log(resource);
    return this.http.post(`${this.endpoint}user/activate`, resource)
      .map(response => response)
      .catch(this.handleError);
  }

  emailPrivacy(resource) {
    console.log(resource);
    return this.http.post(`${this.endpoint}user/emailprivacy`, resource)
      .map(response => response)
      .catch(this.handleError);
  }

  messagePrivacy(resource) {
    console.log(resource);
    return this.http.post(`${this.endpoint}user/privacymessage`, resource)
      .map(response => response)
      .catch(this.handleError);
  }

  location(resource) {
    console.log(resource);
    return this.http.post(`${this.endpoint}user/location`, resource)
      .map(response => response)
      .catch(this.handleError);
  }



  deleteAccount() {
    return this.http.post(`${this.endpoint}user/delete`, '')
      .map(response => response)
      .catch(this.handleError);
  }



  update(resource) {
    return this.http.patch(this.url + '/' + resource.id, JSON.stringify({ isRead: true }))
      .map(response => response)
      .catch(this.handleError);
  }

  delete(id) {
    return this.http.delete(this.url + '/' + id)
      .map(response => response)
      .catch(this.handleError);
  }

  avater(resource) {
    return this.http.post(`${this.endpoint}profile/avater`, resource)
      .map(response => response)
      .catch(this.handleError);
  }

  public handleError(error: Response) {
    if (error.status === 400) {
      return throwError(new BadInput(error));
    }

    if (error.status === 404) {
      return throwError(new NotFoundError(error));
    }

    return throwError(new AppError(error));
  }
}

