import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { DataService } from '../services/data.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( private services: DataService, private router: Router) { }

  canActivate() {
    if ( localStorage.getItem('x-auth-token') ) {
      return true;
    }

    this.router.navigate(['/signin']);
    return false;

  }
}
