import { DataService } from '../services/data.service';
import { HttpClient , HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';


@Injectable({
  providedIn: 'root'
})

export class UserService extends DataService {

  constructor(http: HttpClient) {
    super('http://localhost:3000/api/user', http);
   }

  activateEmail(resource) {
    return this.http.put('http://localhost:3000/api/user/activate', resource)
      .map(response => response)
      .catch(this.handleError);
  }

  addProfile(resource) {
    return this.http.post('http://localhost:3000/api/profile', resource)
      .map(response => response)
      .catch(this.handleError);
  }

  updateProfile(resource) {
    return this.http.post('http://localhost:3000/api/profile', resource)
      .map(response => response)
      .catch(this.handleError);
  }

}
