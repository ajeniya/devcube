import { HttpClient , HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class CountryService extends DataService  {

  constructor(http: HttpClient) {
    super('http://localhost:3000/api/country', http);
  }

  getAll() {
    return this.http.get(this.url)
      .map(response => response)
      .catch(this.handleError);
  }
}
