import { HttpClient , HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class AnswerService extends DataService {
  public endpoint = 'http://localhost:3000/api/';

  constructor(http: HttpClient) {

    super('http://localhost:3000/api/answer', http);

  }

  addAnswer(resource) {
    console.log(resource);
    return this.http.post(`${this.endpoint}answer`, resource)
      .map(response => response)
      .catch(this.handleError);
  }

  getAllanswer() {
    return this.http.get(`${this.endpoint}answer`)
      .map(response => response)
      .catch(this.handleError);
  }

  getSingleanswer(resource) {
    return this.http.get(`${this.endpoint}answer/${resource}`)
      .map(response => response)
      .catch(this.handleError);
  }
}
