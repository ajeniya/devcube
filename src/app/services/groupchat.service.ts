import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class GroupchatService {

  constructor(private socket: Socket) {
  }

  getDocument(id: string) {
    this.socket.emit('getDoc', id);
  }


}
