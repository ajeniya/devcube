import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

const getAuthToken = localStorage.getItem('x-auth-token');


export class AuthInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const authToken = getAuthToken;
        console.log(authToken);

        if (getAuthToken) {
            const token = req.clone({headers: req.headers.set('x-auth-token', authToken)});
            return next.handle(token);
        }

        if (!getAuthToken) {
            const token = req.clone({headers: req.headers.set('x-auth-token', '')});
            return next.handle(token);
        }

    }
}