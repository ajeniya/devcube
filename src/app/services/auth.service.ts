import { HttpClient , HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { DataService } from '../services/data.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService extends DataService {

  login(resource) {
    return this.http.post('http://localhost:3000/api/auth/login', resource)
      .map(response => response)
      .catch(this.handleError);
  }

}
