import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { RoutingStateService } from '../services/routing-state.service';

@Injectable({
  providedIn: 'root'
})
export class LogoutGuardService implements CanActivate {
  previousRoute: string;

  constructor( private services: DataService, private router: Router,
               private routingState: RoutingStateService) { }

  canActivate() {
    if (this.services.isLoggedIn()) {
      this.previousRoute = this.routingState.getPreviousUrl();
      return false;
    }

    this.router.navigate(['/signin']);

  }

}
