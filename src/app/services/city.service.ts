import { HttpClient , HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class CityService extends DataService {

  constructor(http: HttpClient) {
    super('http://localhost:3000/api/city', http);
  }

  getCity(resource) {
    const body: any = { 'state_id': resource };
    return this.http.post('http://localhost:3000/api/city/cities', body)
      .map(response => response)
      .catch(this.handleError);
  }
}
