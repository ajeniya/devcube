import { HttpClient , HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService extends DataService {

  constructor(http: HttpClient) {
    super('http://localhost:3000/api/profile', http);
  }

}
