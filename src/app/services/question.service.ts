import { HttpClient , HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionService extends DataService {
  public endpoint = 'http://localhost:3000/api/';

  constructor(http: HttpClient) {

    super('http://localhost:3000/api/question', http);

  }


  addQuestion(resource) {
    console.log(resource);
    return this.http.post(`${this.endpoint}question`, resource)
      .map(response => response)
      .catch(this.handleError);
  }

  getAllQuestion() {
    return this.http.get(`${this.endpoint}question`)
      .map(response => response)
      .catch(this.handleError);
  }

  getSingleQuestion(resource) {
    return this.http.get(`${this.endpoint}question/${resource}`)
      .map(response => response)
      .catch(this.handleError);
  }

  getSingleQuestioncomment(resource) {
    return this.http.get(`${this.endpoint}question_comment/${resource}`)
      .map(response => response)
      .catch(this.handleError);
  }

  addSingleQuestioncomment(resource) {
    return this.http.post(`${this.endpoint}question_comment`, resource)
      .map(response => response)
      .catch(this.handleError);
  }

  questionUpvote(resource) {
    return this.http.post(`${this.endpoint}question/upvote`, resource)
      .map(response => response)
      .catch(this.handleError);
  }

  questionDownvote(resource) {
    return this.http.post(`${this.endpoint}question/downvote`, resource)
      .map(response => response)
      .catch(this.handleError);
  }

  questionCommentUpvote(resource) {
    return this.http.post(`${this.endpoint}question_comment/upvote`, resource)
      .map(response => response)
      .catch(this.handleError);
  }


  questionCommentDownvote(resource) {
    return this.http.post(`${this.endpoint}question_comment/downvote`, resource)
      .map(response => response)
      .catch(this.handleError);
  }

  

}
