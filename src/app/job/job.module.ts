import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobRoutingModule } from './job-routing.module';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';
import { DetailComponent } from './detail/detail.component';

@NgModule({
  declarations: [HomeComponent, EditComponent, AddComponent, DetailComponent],
  imports: [
    CommonModule,
    JobRoutingModule
  ]
})
export class JobModule { }
