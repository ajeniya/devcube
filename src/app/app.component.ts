import { Component, OnInit } from '@angular/core';
import { UserService } from '../app/services/user.service';
import { DataService } from '../app/services/data.service';
import { HeaderComponent } from './ui/header/header.component';
import { ChatService } from '../app/services/chat.service';
import { RouterOutlet } from '@angular/router';

import { slideInAnimation } from './animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ slideInAnimation ]
})
export class AppComponent implements OnInit {
  isLoggedIn;

  item: boolean;
  subscription: any;

  constructor( private service: UserService, private chat: ChatService,
     private dataServices: DataService, ) {
    this.isLoggedIn = service.isLoggedIn();
  }

  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  ngOnInit() {
    // this.chat.messages.subscribe(msg => {
    //   console.log(msg);
    // });
  }

  // sendMessage() {
  //   this.chat.sendMsg("Test Message");
  // }



}
