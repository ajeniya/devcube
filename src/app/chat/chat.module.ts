import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routes } from './chat-routing.module';
import { ChatRoutingModule } from './chat-routing.module';
import { HomeComponent } from './home/home.component';
import { GroupComponent } from './group/group.component';
import { PrivateComponent } from './private/private.component';

import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild(routes)
   ],
  declarations: [HomeComponent, GroupComponent, PrivateComponent],
 
})
export class ChatModule { }
