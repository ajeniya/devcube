import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PrivateComponent } from './private/private.component';
import { GroupComponent } from './group/group.component';


export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'group', component: GroupComponent },
  { path: 'private', component: PrivateComponent },
];

export class ChatRoutingModule { }
